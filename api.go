package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func writeJSON(w http.ResponseWriter, status int, v any) error {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(v)
}

type apiFunc func(http.ResponseWriter, *http.Request) error
type ApiError struct {
	Error string
}

func makeHTTPHandleFunc(f apiFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := f(w, r); err != nil {
			writeJSON(w, http.StatusBadRequest, ApiError{Error: err.Error()})
		}
	}
}

type APIServer struct {
	listenAddr string
}

func NewAPIServer(listenAddr string) *APIServer {
	return &APIServer{listenAddr: listenAddr}
}

func (s *APIServer) Run() {
	router := mux.NewRouter()

	router.HandleFunc("/upcoming", makeHTTPHandleFunc(s.handleGetUpcomingEvents)).Methods("GET")
	log.Println("UFC API running on port: ", s.listenAddr)
	http.ListenAndServe(s.listenAddr, router)
}

func (s *APIServer) handleEvent(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "GET" {
		return s.handleGetUpcomingEvents(w, r)
	}

	return nil
}

func (s *APIServer) handleGetFormerEvents(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (s *APIServer) handleGetUpcomingEvents(w http.ResponseWriter, r *http.Request) error {
	return nil
}
